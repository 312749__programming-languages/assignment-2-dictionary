%define DICTIONARY_POINTER_SIZE 8
%define USER_INPUT_MAX_LENGTH 255
%include "words.inc"

section .text
extern  exit
extern  string_length
extern  print_string
extern  print_error
extern  print_char
extern  print_newline
extern  print_uint
extern  print_int
extern  string_equals
extern  read_char
extern  read_word
extern  parse_uint
extern  parse_in
extern  string_copy

extern  find_word

section .rodata
no_such_key_exception_msg: db 'No such key in dictionary', 0
too_long_input_exception_msg: db 'Too long input given', 0

section .text
global _start

_start:
  push  rbp
  mov   rbp, rsp
  sub   rsp, USER_INPUT_MAX_LENGTH + 1 ; length with null terminator
  mov   rdi, rsp
  mov   rsi, USER_INPUT_MAX_LENGTH
  call  read_word

  push  rdx
  test  rdx, rdx
  jz    .too_long_input_given

  mov   rdi, rax
  mov   rsi, DICT_POINTER
  call  find_word
  test  rax, rax
  jz    .no_such_key_in_dict

  pop   rdx
  mov   rdi, rax
  add   rdi, DICTIONARY_POINTER_SIZE
  add   rdi, rdx
  inc   rdi
  call  print_string
  call  print_newline

  mov   rsp, rbp
  pop   rbp
  xor   rdi, rdi
  call  exit

  .too_long_input_given:
    mov   rdi, too_long_input_exception_msg
    call  print_error
    call  print_newline
    pop   rdx
    mov   rsp, rbp
    pop   rbp
    xor   rdi, rdi
    call  exit

  .no_such_key_in_dict:
    mov   rdi, no_such_key_exception_msg
    call  print_error
    call  print_newline
    pop   rdx
    mov   rsp, rbp
    pop   rbp
    xor   rdi, rdi
    call  exit
