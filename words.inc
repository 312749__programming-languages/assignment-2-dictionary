%include "colon.inc"
%ifndef NULL
  %define NULL 0
%endif

section .data

colon "first", first
db "some content of first element", NULL

colon "second", second
db "some content of second element", NULL

colon "third", third
db "some content of third element", NULL

colon "last", last
db "some content of last element", NULL