%define DICTIONARY_POINTER_SIZE 8

section .text

extern string_equals

global find_word
; Description:
;    ---
; Params:
;    rdi: pointer to null terminated string representing dict key
;    rsi: pointer to dictionary start
; Return:
;    rax: pointer to dictionary element satisfying condition or 0 if there is no match in dictionary
find_word:
  push  rdi
  push  rsi
  add   rsi, DICTIONARY_POINTER_SIZE
  call  string_equals
  pop   rsi
  pop   rdi

  test  rax, rax
  jnz   .key_found

  mov   rsi, [rsi]
  test  rsi, rsi
  jnz   find_word
  .key_not_found:
    xor   rax, rax
    ret

  .key_found:
    mov   rax, rsi
    ret