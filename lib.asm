%define SYS_READ    0
%define SYS_WRITE   1
%define SYS_EXIT    60

%define STDIN       0
%define STDOUT      1
%define STDERR      2

%define NULL 0

section .text
global exit
global string_length
global print_string
global print_error
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

; Description:
;    Terminates current process
; Params:
;    rdi: status code
; Return:
;    -
exit:
  mov   rax, SYS_EXIT
  ; status code is set as a param
  syscall

; Description:
;    This subroutine counts null-terminated string length
; Params:
;    rdi: string first char pointer
; Return:
;    rax: string length (without counting null terminator)
string_length:
  xor rax, rax
  .foreach_char:
    cmp   byte [rdi + rax], NULL
    je    .exit
    inc   rax
    jmp   .foreach_char
  .exit:
  ret

; Description:
;    This subroutine prints string to STDOUT
; Params:
;    rdi: string first char pointer
; Return:
;    -
print_string:
  push  rdi
  call  string_length
  pop   rsi
  mov   rdx, rax
  mov   rax, SYS_WRITE
  mov   rdi, STDOUT
  syscall
  ret

; Description:
;    This subroutine prints string to STDERR
; Params:
;    rdi: string first char pointer
; Return:
;    -
print_error:
  push  rdi
  call  string_length
  pop   rsi
  mov   rdx, rax
  mov   rax, SYS_WRITE
  mov   rdi, STDERR
  syscall
  ret

; Description:
;    This subroutine prints new line character to stdout
; Extends: print_char
; Params:
;    -
; Return:
;    -
print_newline:
    mov   rdi, `\n`

; Description:
;    This subroutine prints ASCII character to stdout
; Params:
;    rdi: char
; Return:
;    -
print_char:
  push  rdi
  mov   rsi, rsp
  mov   rdx, 1
  mov   rax, SYS_WRITE
  mov   rdi, STDOUT
  syscall
  pop   rdi
  ret

; Description:
;    This subroutine prints signed 64-bit integer
; Params:
;    rdi: int64
; Return:
;    -
print_int:
  cmp   rdi, 0
  jns   print_uint
  neg   rdi
  push  rdi
  mov   rdi, '-'
  call  print_char
  pop   rdi

; Description:
;    This subroutine prints unsigned 64-bit integer
; Params:
;    rdi: uint64
; Return:
;    -
print_uint:
  mov   r8, rsp
  mov   rax, rdi
  dec   rsp
  mov   byte [rsp], NULL
  .div_loop:
    xor   rdx, rdx
    mov   rcx, 10 ; radix
    div   rcx
    add   rdx, '0'
    dec   rsp
    mov   byte [rsp], dl
    cmp   rax, 0
    jne   .div_loop
  mov   rdi, rsp
  push  r8
  call  print_string
  pop   rsp
  ret

; Description:
;    This subroutine compares two given strings.
;    Returns 1 if strings are equal, 0 otherwise
; Params:
;    rdi: first string first char pointer
;    rsi: second string first char pointer
; Return:
;    rax: 1 if strings are equal else 0
string_equals:
  xor   rcx, rcx
  .foreach_char:
    mov   dl, [rdi + rcx]
    cmp   dl, [rsi + rcx]
    jne   .not_equal
    cmp   dl, NULL
    je    .equal
    inc   rcx
    jmp   .foreach_char

  .not_equal:
    xor   rax, rax
    ret
  .equal:
    mov   rax, 1
    ret

; Description:
;    This subroutine reads single character from stdin.
;    Returns character ASCII code if it read successfully, 0 if End Of Stream is reached
; Params:
;    -
; Return:
;    ax: character ASCII code if character read successfully else 0
read_char:
  mov   rax, SYS_READ
  mov   rdi, STDIN
  push  word 0
  mov   rsi, rsp
  mov   rdx, 1
  syscall

  xor   rax, rax
  pop   ax
  ret

; Description:
;    This subroutine reads word from STDIN to buffer (skips leading whitespace characters).
;    Adds null terminator to string.
;    Stops and returns 0 if word is too long for buffer.
;    On success returns pointer on buffer first character and word length.
;    On failure returns 0.
; Params:
;    rdi: buffer start pointer
;    rsi: buffer size
; Return:
;    rax: buffer start pointer if word parsed successfully else 0
;    rdx: word size if word parsed successfully else 0
read_word:
  push  rbx
  xor   rbx, rbx
  push  r12
  mov   r12, rdi
  push  r13
  mov   r13, rsi
  .read_loop:
    call  read_char
    cmp   rbx, r13
    jge    .out_of_bounds
    cmp   rax, ' '
    je    .space_char
    cmp   rax, `\t`
    je    .space_char
    cmp   rax, `\n`
    je    .space_char
    mov   byte [r12 + rbx], al
    cmp   rax, NULL
    je    .exit
    inc   rbx
    jmp   .read_loop
    .space_char:
      cmp   rbx, 0
      je    .read_loop
      mov   byte [r12 + rbx + 1], NULL
  .exit:
    mov   rdx, rbx
    mov   rax, r12
    pop   r13
    pop   r12
    pop   rbx
    ret
  .out_of_bounds:
    xor   rdx, rdx
    xor   rax, rax
    pop   r13
    pop   r12
    pop   rbx
    ret

; Description:
;    This subroutine tries to parse signed 64-bit integer from the start of given string.
;    On success returns parsed 64-bit integer and its length (including sign).
;    On failure returns 0 instead of length.
; Params:
;    rdi: string first char pointer
; Return:
;    rax: parsed int64
;    rdx: uint64 length if int64 parsed successfully
parse_int:
  cmp   byte [rdi], '-'
  jne   parse_uint
  push  rdi
  inc   rdi
  call  parse_uint
  pop   rdi
  cmp   rdx, 0
  je   .error
  .exit:
    neg   rax
    inc   rdx
    ret
  .error:
    xor   rdx, rdx
    ret

; Description:
;    This subroutine tries to parse unsigned 64-bit integer from the start of given string.
;    On success returns parsed 64-bit integer and its length.
;    On failure returns 0 instead of length.
; Params:
;    rdi: string first char pointer
; Return:
;    rax: parsed uint64
;    rdx: uint64 length if uint64 parsed successfully
parse_uint:
  xor   rax, rax
  xor   rdx, rdx
  mov   r8, 10
  .foreach_char:
    movzx rsi, byte [rdi + rdx]
    cmp   rsi, '0'
    jb    .exit
    cmp   rsi, '9'
    ja    .exit
    sub   rsi, '0'
    push  rdx
    mul   r8
    add   rax, rsi
    pop   rdx
    inc   rdx
    jmp   .foreach_char
  .exit:
  ret

; Description:
;    This subroutine tries to copy given string to buffer.
;    On success returns string length.
;    On failure returns 0.
; Params:
;    rdi: string first char pointer
;    rsi: buffer first char pointer
;    rdx: buffer size
; Return:
;    rax: string length if string copied successfully else 0
string_copy:
  push  rdi
  push  rsi
  push  rdx
  call  string_length
  inc   rax ; string_length doesn't count null terminator (like strlen in C)
  pop   rdx
  pop   rsi
  pop   rdi
  cmp   rax, rdx
  jg    .out_of_bounds
  xor   rcx, rcx
  .foreach_char:
    movzx r8, byte [rdi + rcx]
    mov   [rsi + rcx], r8
    inc   rcx
    cmp   r8, NULL
    jne   .foreach_char
  mov   rax, rcx
  ret

  .out_of_bounds:
  xor   rax, rax
  ret
