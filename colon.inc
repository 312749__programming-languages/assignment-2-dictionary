%ifndef NULL
  %define NULL 0
%endif

%define DICT_POINTER 0
%macro colon 2
  %%current:
    dq DICT_POINTER
    db %1, NULL
  %2:

  %define DICT_POINTER %%current
%endmacro