ASM_COMPILER = nasm
ASM_COMPILER_FLAGS = -f elf64 -g
LINKER = ld
RM = rm
RM_FLAGS = -rf
TARGET = main

link: main.o dict.o lib.o
	$(LINKER) -o $(TARGET) $^

%.o: %.asm
	$(ASM_COMPILER) $(ASM_COMPILER_FLAGS) -o $@ $<

clean:
	$(RM) $(RM_FLAGS) $(TARGET) *.o

.PHONY: clean